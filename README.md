

```
az group deployment create --name ClouderaClusterDeployment  --resource-group RG-CLOUDERA-DEV  --template-uri "https://gitlab.com/greg.dolniak/azure-cloudera/-/raw/linked_templates/deployment.json" --verbose --handle-extended-json-format --parameters parameters-dev.json --subscription
```